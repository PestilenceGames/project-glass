using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForgeBase : Interactable
{
    [SerializeField] private float burnTime;
    public ForgeBase activeForge;
    private bool isBurning;
    public float temperatureChangePerSecond;

    [Header("Lights and Effects")]
    [SerializeField] private ParticleSystem effect;
    [SerializeField] private Light forgeLight;
    [Tooltip("Range from this as positive or negative")]

    [SerializeField] private GameObject fireSound;
    public float flickerIntervals;
    public float flickerTime;
    public float flickerRange;
    public float flickerSmoothing;
    private float lightIntensityBase;
    private float targetIntensity;

    private void Start()
    {
        SetForgeEffectLifetime(0.5f);
        lightIntensityBase = forgeLight.intensity;
        StartCoroutine(Flicker());
    }


    public override void StockEnter(StockManager _currentStock)
    {
        activeForge = this;
        isBurning = true;
        StartCoroutine(Forging(_currentStock));
        // Debug.Log("Forge In");


        SetForgeEffectLifetime(3f);
        fireSound.SetActive(true);
    }
    public override void StockLeave(StockManager _stockScript)
    {
        // base.StockLeave(_stockScript);
        isBurning = false;
        SetForgeEffectLifetime(0.5f);
        fireSound.SetActive(false);
        // Debug.Log("Forge Leave");
    }


    IEnumerator Forging(StockManager stockScript)
    {
        float waitTime = burnTime;

        while (isBurning)
        {
            if (waitTime > 0)
                waitTime -= Time.deltaTime;
            else
            {
                waitTime = burnTime;
                // UseForge(stockScript);
                activeForge.UseForge(stockScript);
                // isBurning = false;
            }
            yield return null;
        }
    }

    IEnumerator Flicker()
    {
        yield return new WaitForSeconds(Random.Range(flickerIntervals - flickerTime, flickerIntervals + flickerTime));
        targetIntensity = Random.Range(lightIntensityBase - flickerRange, lightIntensityBase + flickerRange);


        StartCoroutine(Flicker());
    }

    private void FixedUpdate()
    {
        forgeLight.intensity = Mathf.Lerp(forgeLight.intensity, targetIntensity, flickerSmoothing * Time.deltaTime);
    }

    private void SetForgeEffectLifetime(float lifetime)
    {
        var main = effect.main;
        main.startLifetime = lifetime;
    }

    public virtual void UseForge(StockManager stockScript)
    {
        // Debug.Log("called");
        // stockScript.ChangeTemperature(temperatureChangePerSecond);
    }

    

}
