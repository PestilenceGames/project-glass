using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DustBin : Interactable
{
    [SerializeField] private GameObject stock;


    
    public override void StockEnter(StockManager _currentStock)
    {
        base.StockEnter(_currentStock);
        RespawnStock(_currentStock.transform, _currentStock);
        Debug.Log("Dustbin enter");
    }

    private void RespawnStock(Transform _stockTransform, StockManager _currentStock)
    {
        _currentStock.ReturnStockToStart();
        _stockTransform.GetComponent<Rigidbody>().velocity = Vector3.zero;
        _currentStock.ClearAllStatusses();
    
        _currentStock.SetSteelTemperature(0f);
        _currentStock.InitializeStates();
    }



}
