using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anvil : Interactable
{

    [SerializeField] private StatusEffects.ScriptableStatusEffects anvilSection;

    public override void StockEnter(StockManager _currentStock)
    {
        _currentStock.AddNewStatus(anvilSection);
    }

    public override void StockLeave(StockManager _currentStock)
    {
        _currentStock.RemoveStatusIfPresent(anvilSection);
    }

}
