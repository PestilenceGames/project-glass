using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireForge : ForgeBase
{
    [Header("Status Effects")]
    [SerializeField] private StatusEffects.ScriptableStatusEffects primed;
    [SerializeField] private StatusEffects.ScriptableStatusEffects plyable;
    [SerializeField] private StatusEffects.ScriptableStatusEffects whiteHot;
    [SerializeField] private StatusEffects.ScriptableStatusEffects pigsteel;
    [SerializeField] private StatusEffects.ScriptableStatusEffects dirty;
    private float steelHeat;
    
    public override void UseForge(StockManager _currentStock)
    {
        steelHeat = _currentStock.GetTemperature;
        
        if (_currentStock.CheckForstatus(primed) || steelHeat > 0.5f)
            _currentStock.SetSteelTemperature(temperatureChangePerSecond * 2, 1f);
            // _currentStock.SetSteelTemperatureAndColour(temperatureChangePerSecond * 2, 1f, true);
            // _currentStock.ChangeTemperature(temperatureChangePerSecond, 1);
        else
            _currentStock.SetSteelTemperature(temperatureChangePerSecond, 0.5f);
            // _currentStock.SetSteelTemperatureAndColour(temperatureChangePerSecond, 0.5f, true);
            // _currentStock.ChangeTemperature(temperatureChangePerSecond, 0.5f);

        steelHeat = _currentStock.GetTemperature;


        // base.UseForge(_currentScript);
        // Debug.Log("Fire ");

        if (steelHeat > 0.4 && !_currentStock.CheckForstatus(plyable))
            _currentStock.AddNewStatus(plyable);


        if (steelHeat > 0.9 && !_currentStock.CheckForstatus(whiteHot))
        {
            _currentStock.RemoveStatusIfPresent(primed);
            _currentStock.AddNewStatus(whiteHot);
        }

        if (!_currentStock.CheckForstatus(pigsteel) && _currentStock.CheckForstatus(plyable) && _currentStock.CheckForstatus(dirty))
        {
            _currentStock.RemoveStatusIfPresent(dirty);
            _currentStock.AddNewStatus(pigsteel);
        }

    }

    public override void StockEnter(StockManager _currentStock)
    {
        base.StockEnter(_currentStock);
        // Debug.Log("Fire In");
    }

    public override void StockLeave(StockManager _stockScript)
    {
        base.StockLeave(_stockScript);
        // Debug.Log("Fire Out");
    }




}
