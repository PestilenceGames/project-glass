using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceForge : ForgeBase
{

    [SerializeField] private StatusEffects.ScriptableStatusEffects flaked;

    public override void UseForge(StockManager _currentStock)
    {
        base.UseForge(_currentStock);
        _currentStock.SetSteelTemperature(temperatureChangePerSecond, 1f);

        if (_currentStock.CheckForstatus(flaked) && _currentStock.GetTemperature >= 0.2f)
            _currentStock.RemoveStatusIfPresent(flaked);

        // _currentStock.SetSteelTemperatureAndColour(temperatureChangePerSecond, 1f, true);
        // _currentStock.ChangeTemperature(temperatureChangePerSecond, 1f);
        // Debug.Log("Icing");
    }

    public override void StockEnter(StockManager _currentStock)
    {
        base.StockEnter(_currentStock);
        // Debug.Log("Ice In");
    }

    public override void StockLeave(StockManager _stockScript)
    {
        base.StockLeave(_stockScript);
        // Debug.Log("Ice Out");
    }
    

}
