using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oil : Interactable
{
    [SerializeField] StatusEffects.ScriptableStatusEffects checkForWhiteHot;
    [SerializeField] StatusEffects.ScriptableStatusEffects quenched;
    public override void StockEnter(StockManager _currentStock)
    {
        base.StockEnter(_currentStock);
        // Debug.Log("Oil enter");
        _currentStock.SetSteelTemperature(0f);

        if (_currentStock.CheckForstatus(checkForWhiteHot) && _currentStock.forged)
            _currentStock.AddNewStatus(quenched);

    }

}
