using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopTable : MonoBehaviour
{
    [SerializeField] private string stockTag = "Player";

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(stockTag))
        {
            ShopUIManager.Instance.FinishCheck(other.transform.GetComponent<StockManager>());
        }
    }

}
