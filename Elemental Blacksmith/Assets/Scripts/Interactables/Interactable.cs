using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    [Header("Refferences")]
    public LayerMask stockLayer;
    [HideInInspector]
    public string stockTag = "Stock";
    [HideInInspector]
    public StockManager currentStock;
    [SerializeField] List<AudioClip> barrelSounds = new List<AudioClip>();


    [Header("Status Effects")]
    [SerializeField] private List<StatusEffects.ScriptableStatusEffects> addStatusses = new List<StatusEffects.ScriptableStatusEffects>();
    [SerializeField] private List<StatusEffects.ScriptableStatusEffects> removeStatusses = new List<StatusEffects.ScriptableStatusEffects>();


    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(stockTag))
        {
            currentStock = other.transform.GetComponent<StockManager>();
            if (currentStock != null)
                StockEnter(currentStock);
            else
                Debug.Log("Stock Script not found");

                
            if (barrelSounds.Count != 0)
                SoundManager.Instance.PlayRandomClipFromList(barrelSounds);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // if (other.transform.CompareTag(stockTag))
        // Debug.Log("Outs");
        StockLeave(other.transform.GetComponent<StockManager>());
    }

    public virtual void StockEnter(StockManager _currentStock)
    {
        // Debug.Log("Interactable stock enter");
        if (removeStatusses.Count > 0)
        {
            foreach (var _status in removeStatusses)
            {
                _currentStock.RemoveStatusIfPresent(_status);
            }
        }

        if (addStatusses.Count > 0)
        {
            foreach (var _status in addStatusses)
            {
                _currentStock.AddNewStatus(_status);
            }
        }

    }

    public virtual void StockLeave(StockManager _stockScript)
    {

    }

}
