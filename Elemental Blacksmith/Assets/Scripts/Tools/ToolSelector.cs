using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolSelector : MonoBehaviour
{

    [SerializeField] private List<GameObject> tools = new List<GameObject>();
    private GameObject activeTool;
    private Tool currentTool;
    [SerializeField] private int toolIndex;

    private void Start()
    {
        DisableAllTools();
        tools[0].SetActive(true);
        toolIndex = 0;
    }

    public void DisableAllTools()
    {
        foreach (var _tool in tools)
        {
            _tool.SetActive(false);
            _tool.transform.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (Input.GetAxisRaw("Mouse ScrollWheel") > 0)
            SelectNextTool();
        else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0)
            SelectPreviousTool();
    }

    private void SelectNextTool()
    {
        if (toolIndex < tools.Count - 1)
            toolIndex++;
        else
            toolIndex = 0;
        ActivateToolAtIndex(toolIndex);
    }
    private void SelectPreviousTool()
    {
        if (toolIndex == 0)
            toolIndex = tools.Count - 1;
        else
            toolIndex--;
        ActivateToolAtIndex(toolIndex);
    }

    private void ActivateToolAtIndex(int _index)
    {
        DisableAllTools();
        tools[_index].SetActive(true);
    }
}
