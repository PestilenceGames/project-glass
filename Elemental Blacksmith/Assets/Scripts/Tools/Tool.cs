using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool : MonoBehaviour
{
    public ToolsManager toolsManager;
    private bool overStock;

    public virtual void UseToolPress()
    {
        // Debug.Log("Tool Used Once");
    }

    public virtual void UseToolHold()
    {
        // Debug.Log("Tool Held");
    }


    public virtual void StopUsingTool()
    {
        // Debug.Log("Tool Stop");
    }
}
