using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolsManager : MonoBehaviour
{
    [Header("Refferrences")]
    public Transform octagonCursor;
    public LayerMask stockLayer;
    public Transform worldCenter;


    [Header("Tools")]
    [SerializeField] private List<Tool> tools = new List<Tool>();
    public bool overStock;
    [SerializeField] private float sphereCastRadius = 0.5f;


    [HideInInspector] public RaycastHit stockHit;
    [HideInInspector] public Vector3 stockHitPoint;
    private Tool currentTool;
    private int toolIndex;
    [HideInInspector] public GameObject activeStockObject;
    [HideInInspector] public StockManager currentStock;
    [HideInInspector] public Rigidbody activeStockRB;
    [HideInInspector] public Vector3 groundHitPoint;

    private void Start()
    {
        DisableAllTools();
        toolIndex = 0;
        SetCurrentTool();
    }

    public void SelectNextTool()
    {
        if (toolIndex < tools.Count - 1)
            toolIndex++;
        else
            toolIndex = 0;
        SetCurrentTool();
        // ActivateToolAtIndex(toolIndex);
    }

    public void SelectPreviousTool()
    {
        if (toolIndex == 0)
            toolIndex = tools.Count - 1;
        else
            toolIndex--;
        SetCurrentTool();
        // ActivateToolAtIndex(toolIndex);
    }

    public void SetCurrentTool()
    {
        currentTool = tools[toolIndex];
        DisableAllTools();
        currentTool.transform.gameObject.SetActive(true);
    }

    public void DisableAllTools()
    {
        foreach (var _tool in tools)
        {
            _tool.transform.gameObject.SetActive(false);
        }
    }



    public void CurrentToolPress()
    {
        currentTool.UseToolPress();
    }
    public void CurrentToolRealease()
    {
        currentTool.StopUsingTool();
    }
    public void CurrentToolHold()
    {
        currentTool.UseToolHold();
    }


    private void FixedUpdate()
    {
        LookForStock();
    }

    public void LookForStock()
    {
        if (Physics.SphereCast(octagonCursor.position + Vector3.up, sphereCastRadius, Vector3.down, out stockHit, 10f, stockLayer)) //&& !overStock) //The overstock is for optimisation
        {
            overStock = true;
            activeStockObject = stockHit.transform.gameObject;
            stockHitPoint = stockHit.point;
            currentStock = activeStockObject.GetComponent<StockManager>();
        }
        else
        {
            overStock = false;
            // activeStock = null;
        }
    }
}
