using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tongs : Tool
{
    private bool isHolding = false;


    public override void UseToolPress()
    {
        base.UseToolPress();
        // Debug.Log("Use Tongs");
        HoldSteel();
    }

    public override void UseToolHold()
    {
        base.UseToolHold();
        // Debug.Log("Use Tongs Hold");
    }

    public override void StopUsingTool()
    {
        base.StopUsingTool();
        DropSteel();
        // Debug.Log("Use Tongs Up");
    }

    public void DropSteel()
    {
        isHolding = false;
        toolsManager.activeStockObject.transform.parent = null;
        toolsManager.activeStockObject.transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
        // toolsManager.activeStock.transform.GetComponent<Rigidbody>().useGravity = true;
        toolsManager.activeStockRB.isKinematic = false;
    }

    public void HoldSteel()
    {
        isHolding = true;
        // toolsManager.activeStockRB.useGravity = false;
        toolsManager.activeStockRB.isKinematic = true;
        toolsManager.activeStockObject.transform.parent = toolsManager.octagonCursor.transform;
        toolsManager.activeStockRB.velocity = Vector3.zero;
        toolsManager.activeStockRB.position = toolsManager.octagonCursor.transform.position;
        // toolsManager.activeStock.transform.position = toolsManager.octagonCursor.position;
        toolsManager.activeStockRB.velocity = Vector3.zero;
        // toolsManager.activeStock.transform.position = toolsManager.octagonCursor.transform.position; 
        // toolsManager.activeStock.transform.SetParent(toolsManager.octagonCursor, false);
        StartCoroutine(HoldingSteel());
    }

    IEnumerator HoldingSteel()
    {
        while (isHolding)
        {
            toolsManager.activeStockObject.transform.position = Vector3.Lerp(toolsManager.activeStockObject.transform.position,toolsManager.octagonCursor.transform.position, Time.fixedDeltaTime * 10);
            yield return new WaitForFixedUpdate();
        }
    }

}
