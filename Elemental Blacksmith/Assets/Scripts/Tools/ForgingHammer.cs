using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForgingHammer : Tool
{
    [Header("Hammer Setings")]
    [SerializeField] private float TimeBetweenBlows = 1f;
    private States stockState = new States();
    private bool hammering;

    [Header("Anvil Parts")]
    [SerializeField] private StatusEffects.ScriptableStatusEffects shoe;
    [SerializeField] private StatusEffects.ScriptableStatusEffects flat;
    [SerializeField] private StatusEffects.ScriptableStatusEffects horn;
    [SerializeField] private StatusEffects.ScriptableStatusEffects plyable;

    [Header("Hammer Effects")]
    [SerializeField] private Transform hammerParticles;
    private ParticleSystem hammerEffects;

   [Header("Sounds")]
   [SerializeField] List<AudioClip> hammerSounds = new List<AudioClip>();


    private void Start()
    {
        hammerEffects = hammerParticles.GetComponent<ParticleSystem>();
    }
    public override void UseToolPress()
    {
        base.UseToolPress();
        hammering = true;
        StartCoroutine(Hammering());
        // Debug.Log("Use Hammer");
    }

    public override void UseToolHold()
    {
        base.UseToolHold();
        // Debug.Log("Use Hammer Hold");

    }
    public override void StopUsingTool()
    {
        base.StopUsingTool();
        hammering = false;
        StopCoroutine(Hammering());
        // Debug.Log("Use Hammer Up");
    }

    IEnumerator Hammering()
    {

        float waitTime = TimeBetweenBlows;

        while (hammering)
        {
            if (waitTime > 0)
                waitTime -= Time.deltaTime;
            else
            {
                waitTime = TimeBetweenBlows;
                HammerUse();
            }
            yield return null;
        }
    }

    private void HammerUse()
    {

        if (hammering && toolsManager.currentStock.CheckForstatus(plyable))
        {
            // Play Sound
            SoundManager.Instance.PlayRandomClipFromList(hammerSounds);
            // Debug.Log("Hammer");
            hammerParticles.position = toolsManager.stockHitPoint;
            hammerEffects.Play();

            HammerStockTowardsState();

        }

        // toolsManager.activeStock
    }

    private void HammerStockTowardsState()
    {
        stockState = States.stock;

        if (toolsManager.currentStock.CheckForstatus(flat))
            stockState = States.sword;
        if (toolsManager.currentStock.CheckForstatus(horn))
            stockState = States.axe;
        if (toolsManager.currentStock.CheckForstatus(shoe))
            stockState = States.dagger;
        CreateNewProgressState(stockState);
    }

    private void CreateNewProgressState(States _state)
    {
        toolsManager.currentStock.ForgeTowardsState(1,_state);
    }
}
