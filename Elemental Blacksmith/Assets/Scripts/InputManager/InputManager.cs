using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{
    [SerializeField] UnityEvent OnPressEvent;
    [SerializeField] UnityEvent OnReleaseEvent;
    [SerializeField] UnityEvent OnHoldEvent;
    [SerializeField] UnityEvent OnSelectNextToolEvent;
    [SerializeField] UnityEvent OnSelectPreviousToolEvent;
    [SerializeField] UnityEvent OnPause;

    //I get that this is not the best way to handle input, as the new UNITY INPUT system would be better, this was a workaround to try implement that system.

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !IsMouseOverUI) //Tool Press
            OnPressEvent.Invoke();

        if (Input.GetMouseButtonUp(0) && !IsMouseOverUI) //Tool button up
            OnReleaseEvent.Invoke();

        if (Input.GetMouseButton(0) && !IsMouseOverUI) //Tool Hold
            OnHoldEvent.Invoke();

        if (Input.GetKeyDown(KeyCode.Escape))
            OnPause.Invoke();

        if (Input.GetAxisRaw("Mouse ScrollWheel") > 0) //Scroll for tools
            OnSelectNextToolEvent.Invoke();
        else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0)
            OnSelectPreviousToolEvent.Invoke();
    }

    public bool IsMouseOverUI
    {
        get 
        {
            return EventSystem.current.IsPointerOverGameObject();
        }
    }
}
