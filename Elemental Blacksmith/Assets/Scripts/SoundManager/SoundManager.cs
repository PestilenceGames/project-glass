using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    [SerializeField] private AudioSource soundSource;
    private int range;
    private int selectedSound;

    private void Awake()
    {
        Instance = this;
        soundSource = this.transform.GetComponent<AudioSource>();
    }

    public void PlayRandomClipFromList(List<AudioClip> sounds)
    {
        range = sounds.Count - 1;

        if (range != 0)
        {
            selectedSound = Random.Range(0, range);
            // soundSource.clip = sounds[selectedSound];
            soundSource.PlayOneShot(sounds[selectedSound]);
        }
    }

}
