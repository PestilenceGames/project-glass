using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class StockManager : MonoBehaviour
{
    // [SerializeField] UnityEvent OnStatusActivity;
    public delegate void OnStatusChange(StatusEffects.ScriptableStatusEffects status, States state, bool adding);
    public static event OnStatusChange statusChange;

    [Header("Steel Properties")]
    // [HideInInspector]
    public List<ScriptableObject> statusses = new List<ScriptableObject>();
    [SerializeField] private Material steelMaterial;
    // [SerializeField] private List<ScriptableObject> heatStatuses = new List<ScriptableObject>();
    [Header("Heat related Statusses")]
    [SerializeField] private StatusEffects.ScriptableStatusEffects plyable;
    [SerializeField] private StatusEffects.ScriptableStatusEffects whiteHot;
    [SerializeField] private StatusEffects.ScriptableStatusEffects pigSteel;
    [SerializeField] private StatusEffects.ScriptableStatusEffects dirty;
    [SerializeField] private StatusEffects.ScriptableStatusEffects forgedStatus;
    [SerializeField] private StatusEffects.ScriptableStatusEffects flaked;
    [SerializeField] private StatusEffects.ScriptableStatusEffects hardened;
    [SerializeField] private StatusEffects.ScriptableStatusEffects soft;

    [Header("Heat")]
    [SerializeField] private float steelTemperature;
    [SerializeField] private float heatLossRate;
    [SerializeField] private float heatLossAmount;
    private bool hot;

    [Header("Stock States")]
    [SerializeField] private List<GameObject> stateMeshes = new List<GameObject>();
    private List<StateProgress> stateProgress = new List<StateProgress>();
    [HideInInspector]
    public bool forged;
    [HideInInspector]
    public States currentState = new States();
    private Vector3 startPos;




    private void Start()
    {
        startPos = this.transform.position;
        SetSteelTemperature(0f);

        InitializeStates();
    }

    public void InitializeStates()
    {
        stateProgress.Clear();

        for (var i = 0; i < stateMeshes.Count; i++)
        {
            StateProgress _newState = new StateProgress();
            _newState.progressTowardsState = 0;
            _newState.stockState = (States)i;

            stateProgress.Add(_newState);
        }
        DisableAllStatesExcept(0);
        forged = false;
    }

    public void ClearAllStatusses()
    {
        statusses.Clear();
    }

    public void AddNewStatus(StatusEffects.ScriptableStatusEffects _status)
    {
        if (!statusses.Contains(_status))
        {
            statusses.Add(_status);
            DisplayNewEffect(_status, true);
        }
        // Debug.Log($"Try to add {_status}");
    }

    public void RemoveStatusIfPresent(StatusEffects.ScriptableStatusEffects _status)
    {
        if (statusses.Contains(_status))
        {
            statusses.Remove(_status);
            DisplayNewEffect(_status, false);
        }
        // else
        //     Debug.Log("This status was not present");
    }

    public bool CheckForstatus(StatusEffects.ScriptableStatusEffects _statusToCheck)
    {
        bool _hasStatus = false;

        if (statusses.Contains(_statusToCheck))
            _hasStatus = true;

        return _hasStatus;
    }

    public void SetSteelTemperature(float temperature, float clamp)
    {
        steelTemperature += temperature;

        steelTemperature = Mathf.Clamp(steelTemperature, 0f, clamp);
        SetSteelColour();
    }
    public void SetSteelTemperature(float temperature)
    {
        steelTemperature = temperature;
        steelTemperature = Mathf.Clamp(steelTemperature, 0f, 1f);
        SetSteelColour();
    }

    private void SetSteelColour()
    {
        hot = true;
        StartCoroutine(Cooling());

        steelTemperature = Mathf.Clamp(steelTemperature, 0f, 1f); //Safety Redundancy
        steelMaterial.SetFloat("_temp", steelTemperature);
        DisplaySteelTemp.Instance.SetSteelFillAmount = steelTemperature;
    }

    public float GetTemperature
    {
        get
        {
            return steelTemperature;
        }
    }

    IEnumerator Cooling()
    {
        float waitTime = heatLossRate;

        while (hot)
        {
            // Debug.Log("Cooling");
            if (waitTime > 0)
                waitTime -= Time.deltaTime;
            else
            {
                waitTime = heatLossRate;
                Cool();
            }
            yield return new WaitForFixedUpdate();
        }
    }

    private void Cool()
    {
        // SetSteelTemperatureAndColour(heatLossAmount, 1f, true);
        steelTemperature += heatLossAmount;
        steelTemperature = Mathf.Clamp(steelTemperature, 0f, 1f); //Safety Redundancy
        steelMaterial.SetFloat("_temp", steelTemperature);

        DisplaySteelTemp.Instance.SetSteelFillAmount = steelTemperature;

        if (steelTemperature < 0.8 && CheckForstatus(whiteHot))
        {
            RemoveStatusIfPresent(whiteHot);

            if (forged)
            {
                AddNewStatus(hardened);
                RemoveStatusIfPresent(soft);
            }
            else
                AddNewStatus(flaked);

        }
        if (steelTemperature < 0.3 && CheckForstatus(plyable))
        {
            RemoveStatusIfPresent(plyable);

            if (!forged)
                AddNewStatus(flaked);
        }


        if (steelTemperature <= 0)
            hot = false;
    }

    public void ReturnStockToStart()
    {
        this.transform.position = startPos + Vector3.up;
    }

    public void DisplayNewEffect(StatusEffects.ScriptableStatusEffects status, bool adding)
    {
        // OnStatusActivity.Invoke();

        if (statusChange != null)
        {
            statusChange(status, currentState, adding);
        }
    }

    public void DisableAllStatesExcept(int index)
    {
        foreach (GameObject _object in stateMeshes)
        {
            _object.SetActive(false);
        }
        if (index >= 0 && index < stateMeshes.Count)
        {
            stateMeshes[index].SetActive(true);
            currentState = (States)index;
        }
        else
            Debug.LogError("Index not within the list bounds.");
    }

    public void ForgeTowardsState(int amount, States state)
    {
        if (!forged)
        {
            foreach (var stateProgress in stateProgress)
            {
                // Debug.Log(stateProgress.stockState);
                if (stateProgress.stockState == state)
                {
                    stateProgress.progressTowardsState++;
                    // Debug.Log("Forging");
                    if (stateProgress.progressTowardsState > 3 && state != States.stock)
                    {
                        DisableAllStatesExcept((int)state);
                        AddNewStatus(forgedStatus);
                        forged = true;
                    }
                }
            }
        }

    }
}



[System.Serializable]
public class StateProgress
{
    public States stockState = new States();
    public int progressTowardsState;
}

[System.Serializable]
public enum States
{
    stock,
    sword,
    dagger,
    axe
}