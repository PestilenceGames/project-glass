using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanWorldForCursorPlacement : MonoBehaviour
{

    [Header("Cursor Settings")]
    [SerializeField] private Transform octagonCursor;
    [SerializeField] private LayerMask cursorGrid;
    [SerializeField] private Camera mainCam;
    [SerializeField] private float cursorPositionY = 1f;
    [SerializeField] private float lerpSpeed = 0.3f;
    [SerializeField] private Material green;
    [SerializeField] private Material grey;
    [SerializeField] private Renderer octogonCursorMesh;
    private RaycastHit gridHit;
    private Vector3 gridHitPoint;
    private Vector3 lerpTarget;
    private Ray screenToWorld;

    [Header("Line Renderer Settings")]
    [SerializeField] private float lineRenderStartPositionY;
    [SerializeField] private LayerMask floorLayer;
    [SerializeField] private LayerMask interactableLayer;
    private RaycastHit floorHit;
    private Vector3 lineRenderStartPoint;
    private Vector3 floorHitPoint;
    private LineRenderer line;

    private void Start()
    {
        line = this.transform.GetComponent<LineRenderer>();
    }

    private void Update()
    {
        { //CURSOR PLACEMENT
            screenToWorld = mainCam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(screenToWorld, out gridHit, 1000f, cursorGrid, QueryTriggerInteraction.Collide))
            {
                // Debug.Log("hit");
                gridHitPoint = gridHit.point;
                lerpTarget = new Vector3(Mathf.Floor(gridHitPoint.x) + 0.5f, cursorPositionY, Mathf.Floor(gridHitPoint.z) + 0.5f);

                octagonCursor.position = Vector3.Lerp(octagonCursor.position, lerpTarget, lerpSpeed * Time.deltaTime);
                octagonCursor.gameObject.SetActive(true);
            }
            else
            {
                //TODO Deal with Inactive Cursor
            }

        }

        { //LINE RENDERING
            lineRenderStartPoint = new Vector3(octagonCursor.position.x, lineRenderStartPositionY, octagonCursor.position.z);
            line.SetPosition(0, lineRenderStartPoint);

            if (Physics.Raycast(lineRenderStartPoint, Vector3.down, out floorHit, 5f, floorLayer))
            {
                floorHitPoint = floorHit.point;
            }
            line.SetPosition(1, floorHitPoint);
        }

        { //TURN GREEN OVER INTERACTABLE

        }
    }

    private void FixedUpdate()
    {
        if (Physics.Raycast(lineRenderStartPoint, Vector3.down, 10f, interactableLayer, QueryTriggerInteraction.Collide))
                octogonCursorMesh.material = green;
        else
                octogonCursorMesh.material = grey;


    }


}