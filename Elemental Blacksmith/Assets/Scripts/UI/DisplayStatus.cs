using System;
using System.Collections;
using System.Collections.Generic;
using StatusEffects;
using UnityEngine;
using TMPro;

public class DisplayStatus : MonoBehaviour
{

    [SerializeField] private List<TextMeshProUGUI> displays = new List<TextMeshProUGUI>();
    [SerializeField] private float alphaDrop;
    [SerializeField] private Color grey;
    [SerializeField] private Color red;
    [SerializeField] private Color white;
    [SerializeField] private Color green;
    [SerializeField] private Color gold;
    private String displayText;
    private Color displayColor;

    private void OnEnable()
    {
        StockManager.statusChange += EvaluateStatusChange;
    }
    private void OnDisable()
    {
        StockManager.statusChange -= EvaluateStatusChange;
    }
    
    private void Start()
    {
        InitializeDisplays();
    }

    private void InitializeDisplays()
    {
        foreach (TextMeshProUGUI _text in displays)
        {
            _text.text = "";
            _text.color = white;
        }
    }

    private void EvaluateStatusChange(ScriptableStatusEffects status, States state, bool adding)
    {

        if (adding) //ADDING SENTENCES
            displayText = (status.anvilStatus) ?  $"The {state} has been placed on the anvil's {status.status}" : $"The {state} is now {status.status}";

        if (status.Quality < 0)
            displayColor = red;
        if (status.Quality > 0)
            displayColor = green;
        if (status.Quality == 0)
            displayColor = white;
        if (status.heatStatus)
            displayColor = gold;

        if (!adding) //SUBTRACTING SENTENCES (REDUNDANT BUT ADDED FOR READABILITY)
        {
            displayText = (status.anvilStatus) ?  $"The {state} has left the anvil's {status.status}" : $"The {state} is no longer {status.status}";
            displayColor = grey;
        }

        DisplayNewStatus(displayText, displayColor);
    }

    [ContextMenu("This Is it")]
    private void DisplayNewStatus(string displayText, Color displayColor)
    {
        for (int i = displays.Count - 1; i >= 1 ; i--)
        {
            displays[i].text = displays[i-1].text;
            displays[i].color = displays[i-1].color;
        }

        displays[0].text = displayText;
        displays[0].color = displayColor;
    }


}
