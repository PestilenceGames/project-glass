using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplaySteelTemp : MonoBehaviour
{

    public static DisplaySteelTemp Instance;
    [SerializeField] Image steelFill;
    [SerializeField] float targetFillamount;
    private bool fill;


    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        targetFillamount = 0f;
        SetSteelFillAmount = targetFillamount;
    }

    public float SetSteelFillAmount
    {
        set
        {
            targetFillamount = value;
            targetFillamount = Mathf.Clamp(targetFillamount, 0f,1f);
            DisplaySteelFillAmount();
        }
    }

    private void DisplaySteelFillAmount()
    {
        steelFill.fillAmount = targetFillamount;
    }


}
