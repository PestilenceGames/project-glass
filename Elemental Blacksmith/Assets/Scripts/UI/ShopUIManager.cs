using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ShopUIManager : MonoBehaviour
{

    public static ShopUIManager Instance;
    [SerializeField] private GameObject finishCheck;
    [SerializeField] private GameObject shopMenu;
    [SerializeField] private List<GameObject> produce = new List<GameObject>();
    private StockManager currentStock;
    private float weaponPrice = 0;
    private float weaponQuality = 0;

    [Header("Shop Settings")]
    [SerializeField] private TextMeshProUGUI stateText;
    [SerializeField] private TextMeshProUGUI PriceText;
    [SerializeField] private TextMeshProUGUI QualityText;
    [SerializeField] private TextMeshProUGUI PosText;
    [SerializeField] private TextMeshProUGUI negText;

    private string positiveStatusString;
    private string negativeStatusString;

    [SerializeField] List<AudioClip> coinSounds = new List<AudioClip>();

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        InitializeShopMenu();
    }

    private void InitializeShopMenu()
    {
        finishCheck.SetActive(false);
        shopMenu.SetActive(false);
        weaponPrice = 0f;
        weaponQuality = 0f;

        foreach (GameObject _product in produce)
        {
            _product.SetActive(false);
        }

        positiveStatusString = "Positive Effects";
        negativeStatusString = "Negative Effects";
        // posStats.a
    }

    public void FinishCheck(StockManager _currentStock)
    {
        currentStock = _currentStock;
        finishCheck.SetActive(true);
    }
    public void HideFinishCheck()
    {
        currentStock.ReturnStockToStart();
        finishCheck.SetActive(false);
    }

    public void DisplayShop()
    {
        SoundManager.Instance.PlayRandomClipFromList(coinSounds);
        // currentStock.SetSteelTemperature(0f);

        Debug.Log("Display");
        finishCheck.SetActive(false);
        shopMenu.SetActive(true);

        stateText.text = currentStock.currentState.ToString();
        DisplayModel();

        CalculatePriceAndQuality();
    }

    private void CalculatePriceAndQuality()
    {
        foreach (StatusEffects.ScriptableStatusEffects status in currentStock.statusses)
        {
            if (status.Price != 0)
                weaponPrice += status.Price;

            if (status.Quality != 0)
            {
                weaponQuality += status.Quality;

                if ((status.Quality > 0))
                    positiveStatusString += "\n" + status.status;
                else
                    negativeStatusString += "\n" + status.status;
            }
        }

        AssignTexts();
    }

    private void AssignTexts()
    {
        if (weaponPrice < 0)
            weaponPrice = 0;
        if (weaponQuality < 0)
            weaponQuality = 0;

        PriceText.text = $"Price: {weaponPrice}";
        QualityText.text = $"Quality: {weaponQuality}";

        PosText.text = positiveStatusString;
        negText.text = negativeStatusString;
    }

    private void DisplayModel()
    {
        switch (currentStock.currentState)
        {
            case States.stock:
                produce[0].SetActive(true);
                break;
            case States.axe:
                produce[1].SetActive(true);
                break;
            case States.sword:
                produce[2].SetActive(true);
                break;
            case States.dagger:
                produce[3].SetActive(true);
                break;
            default:
                break;
        }

        // produce[(int)currentStock.currentState].SetActive(true);

    }

    public void ResetGame()
    {
        if (currentStock == null)
            currentStock = FindObjectOfType<StockManager>();

        currentStock.ReturnStockToStart();
        currentStock.SetSteelTemperature(0f);
        currentStock.ClearAllStatusses();
        currentStock.InitializeStates();

        InitializeShopMenu();
    }
}
