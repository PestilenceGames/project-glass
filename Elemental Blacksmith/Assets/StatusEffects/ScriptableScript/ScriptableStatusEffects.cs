using UnityEngine;


namespace StatusEffects
{

    [CreateAssetMenu(fileName = "StatusEffects", menuName = "Elemental Blacksmith/StatusEffects", order = 0)]
    public class ScriptableStatusEffects : ScriptableObject
    {

        [Header("Status")]
        public string status;
        public StatusDuration statusDuration = new StatusDuration();
        public int Quality;
        public int Price;
        public bool anvilStatus = false;
        public bool heatStatus = false;

    }


    [System.Serializable]
    public enum StatusDuration
    {
        temporary,
        permanent
    }
}
