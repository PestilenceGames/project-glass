using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseFunctionality : MonoBehaviour
{
    public static bool isPaused;

    [Header("Pause UI")]
    [SerializeField] private GameObject blockoutPanel;
    [SerializeField] private GameObject pauseMenuToggleUI;
    [SerializeField] private GameObject quitCheck;
    [SerializeField] private GameObject controlsScreen;
    [SerializeField] private float fadeDuration;

    private void Start()
    {
        InitializePause();
    }

    private void InitializePause()
    {
        blockoutPanel.SetActive(false);
        quitCheck.SetActive(false);
        pauseMenuToggleUI.SetActive(false);
        HideControls();
        isPaused = false;
    }


    public void TogglePause()
    {
        if (isPaused)
            UnPause();
        else
            Pause();
    }

    private void Pause()
    {
        InitializePause();

        pauseMenuToggleUI.SetActive(true);

        blockoutPanel.SetActive(true);
        Time.timeScale = 0;
        isPaused = true;

        blockoutPanel.transform.GetComponent<Image>().CrossFadeAlpha(0f, 0f, true);
        blockoutPanel.transform.GetComponent<Image>().CrossFadeAlpha(1f, fadeDuration, true);
    }

    public void UnPause()
    {
        pauseMenuToggleUI.SetActive(false);
        Time.timeScale = 1;
        isPaused = false;
        blockoutPanel.transform.GetComponent<Image>().CrossFadeAlpha(0f, fadeDuration, true);
        StartCoroutine(TimeToDisable(fadeDuration));
        HideControls();
    }

    IEnumerator TimeToDisable(float duration)
    {
        yield return new WaitForSeconds(duration);
        blockoutPanel.SetActive(false);
    }

    public void QuitCheck()
    {
        quitCheck.SetActive(true);
    }

    public void QuitCheckCancel()
    {
        quitCheck.SetActive(false);
    }

    public void ShowControls()
    {
        controlsScreen.SetActive(true);
    }

    public void HideControls()
    {
        controlsScreen.SetActive(false);
    }

    public void QuitTheGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    public bool IsGamePaused
    {
        get
        {
            return isPaused;
        }
    }

}
